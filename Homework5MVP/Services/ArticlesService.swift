
import Foundation
import Alamofire
import KVLoading

protocol ArticlesServiceDelegate {
    func didGetArticleSuccess(articles: [Articles])
    func didAddNewArticleSuccess(msg: String)
    func didUpdateArticleSuccess(msg: String)
    func didDeleteArticleSuccess(msg: String)
}

class ArticlesService {
    var delegate: ArticlesServiceDelegate?
    var imageWillUpload : UIImage!
    var imageUrl: String?
    let limit = 15
    let page = 1
    var article_api_url = "http://api-ams.me/v1/api/articles"
    let header = [
        "Authorization": "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=",
        "Accept": "application/json",
        "Content-Type": "application/json"
    ]
    

    func getArticles(page: Int, limit: Int) {
        var articles =  [Articles]()
        Alamofire.request("\(article_api_url)?page=\(page)&limit=\(limit)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if response.result.isSuccess {
                if let json = try? JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as! [String: Any]{
                    let object = json["DATA"] as! NSArray
                    for obj in object{
                        articles.append(Articles(JSON: obj as! [String:Any])!)
                    }
                    self.delegate?.didGetArticleSuccess(articles: articles)
                }
            }else{
                print("Request data not success. Internet slow")
            }
        }
    }
    
    func addAriticle(article: Articles)  {
        let data: [String:Any] = [
            "TITLE": article.title! ,
            "DESCRIPTION": article.description!,
            "AUTHOR": 0,
            "CATEGORY_ID": 0,
            "STATUS": "YOU HAVE BEEN ADD NEW ARTICLE SUCCESSFULLY.",
            //"IMAGE": "https://i.ytimg.com/vi/sBrjJNrMJpE/hqdefault.jpg"
            "IMAGE": article.image ?? "https://i.ytimg.com/vi/sBrjJNrMJpE/hqdefault.jpg"
        ]
        Alamofire.request(self.article_api_url, method: .post, parameters: data, encoding: JSONEncoding.default, headers: self.header).responseJSON{
            (response) in
            if response.result.isSuccess{
                print("Successfully add article!")
                self.delegate?.didAddNewArticleSuccess(msg: "Successfully add article!")
                KVLoading.hide()
            }
            else{
                print("Slow Internet! Please wait a minute.")
            }
        }
        
    }
    
    
    func updateArticle(article: Articles  )  {
        let data: [String:Any] = [
            "TITLE": article.title! ,
            "DESCRIPTION": article.description!,
            "AUTHOR": 0,
            "CATEGORY_ID": 0,
            "STATUS": "YOU HAVE BEEN ADD NEW ARTICLE SUCCESSFULLY.",
            "IMAGE": article.image ?? "https://i.ytimg.com/vi/sBrjJNrMJpE/hqdefault.jpg"
        ]
        print("id \(article.id)")
        Alamofire.request("\(self.article_api_url)/\(article.id)", method: .put, parameters: data, encoding: JSONEncoding.default, headers: self.header).responseJSON{
            (response) in
            if response.result.isSuccess{
                print(" add article!")
                self.delegate?.didAddNewArticleSuccess(msg: "Successfully add article!")
                KVLoading.hide()
                print("-----------------")
                print(response.result.value)
                print("-----------------")
            }
            else{
                print("Slow Internet! Please wait a minute.")
            }
        }
    }
    
    func deleteArticle(articleId: Int) {
        Alamofire.request("\(article_api_url)/\(articleId)", method: .delete, parameters: nil, encoding: JSONEncoding.default, headers:header ).responseJSON{
            (response) in
            if response.result.isSuccess{
                print("success add in service")
                self.delegate?.didDeleteArticleSuccess(msg: "Delete Article Success")
            }else{
                print("slow internet")
            }
        }
    }
}



