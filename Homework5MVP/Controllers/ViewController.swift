
import UIKit
import Kingfisher
import KVLoading

class ViewController: UIViewController {
    var articles: [Articles]?
    static var limit = 15
    static var page = 1
    var refreshStatus = true
    
    var articlesPresenter: ArticlesPresenter?
     var refresh : UIRefreshControl!
    @IBOutlet weak var articleTableView: UITableView!
    
    @IBAction func addNewArticle(_ sender: Any) {
        pushScreen(status: 0, data: Articles())
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.articles = [Articles]()
        self.articlesPresenter = ArticlesPresenter()
        self.articlesPresenter?.delegate = self
        articleTableView.delegate = self
        articleTableView.dataSource = self
        
        refresh = UIRefreshControl()
        refresh.attributedTitle = NSAttributedString(string: "Loading")
        refresh.addTarget(self, action: #selector(ViewController.refreshArticleData), for: UIControl.Event.valueChanged)
        articleTableView.addSubview(refresh)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        KVLoading.show()
        articles = []
        articlesPresenter?.getArticles(page: 1, limit: 15)
        articleTableView.reloadData()
    }
    
    @objc func refreshArticleData() {
        if refreshStatus == true {
            refreshStatus = false
            articles = []
            articlesPresenter?.getArticles(page: 1, limit: 15)
            articleTableView.reloadData()
//            refresh.endRefreshing()
            refresh.willRemoveSubview(refresh)
        }
        
        print("refreshArticleData")
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    
    //define total row
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (articles?.count)!
    }
    
    
    //set article for each row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = articleTableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as! ArticleTableViewCell
        cell.setArticleInformation(article: articles![indexPath.row])
        KVLoading.hide()
        return cell
    }
    
    //view detail
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pushScreen(status: 1, data: articles![indexPath.row])
    }
    
    //delete Article
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = deleteAction(at: indexPath)
        return UISwipeActionsConfiguration(actions: [delete])
        
    }
    func deleteAction(at indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .destructive, title: "Delete"){
            (action, view, completion) in
            self.articlesPresenter?.deleteArticle(articleId: self.articles![indexPath.row].id)
            self.articles?.remove(at: indexPath.row)
            self.articleTableView.deleteRows(at: [indexPath], with: .automatic)
            completion(true)
        }
        action.image = UIImage(named: "Delete")
        action.backgroundColor = .red
        return action
    }
    
    //Edit Article
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let edit = editAction(at: indexPath)
        return UISwipeActionsConfiguration(actions: [edit])
    }
    
    func editAction(at indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .destructive, title: "Edit"){
            (action, view, completion) in
            guard  let view = self.storyboard?.instantiateViewController(withIdentifier: "modifyScreen") as? ModifyArticleViewController else {
                fatalError("View  not found")
            }
            view.status = 2
            view.article = Articles()
            view.article = self.articles![indexPath.row]
            self.navigationController?.pushViewController(view, animated: true)
            //self.pushScreen(status: 2, data: self.articles![indexPath.row])
        }
        action.image = UIImage(named: "Edit")
        action.backgroundColor = .green
        return action
    }
    
    func pushScreen(status: Int, data: Articles) {
        guard  let view = self.storyboard?.instantiateViewController(withIdentifier: "modifyScreen") as? ModifyArticleViewController else {
            fatalError("View  not found")
        }
        view.status = status
        view.article = data
        print(data.id)
        self.navigationController?.pushViewController(view, animated: true)
    }

    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
            ViewController.page += 1
            self.articlesPresenter?.getArticles(page: ViewController.limit, limit: ViewController.page)
            self.articleTableView.reloadData()
    }
}


extension ViewController: ArticlesPresenterDelegate{
    
    func didGetArticleSuccess(articles: [Articles]) {
        self.articles = self.articles! + articles
        articleTableView.reloadData()
        refreshStatus = true
    }
    
    func didDeleteArticleSuccess(msg: String) { }
    
    func didAddNewArticleSuccess(msg: String) { }
    
    func didUpdateArticleSuccess(msg: String) { }
    
   
    
    
}
