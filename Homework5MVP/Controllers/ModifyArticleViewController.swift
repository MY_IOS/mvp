
import UIKit
import Kingfisher
import KVLoading
import Alamofire

class ModifyArticleViewController: UIViewController,UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    var status: Int = 0
    var article: Articles?
    var articleUpdate: Articles?
    var img : UIImage!
    var imageUrl: String?
    @IBOutlet weak var modifyButton: UIBarButtonItem!
    @IBOutlet weak var articleDescriptionTextView: UITextView!
    @IBOutlet weak var articleTitleTextView: UITextView!
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var articleCreatedDateLabel: UILabel!
    @IBOutlet weak var articleImageView: UIImageView!
    var articlePresenter : ArticlesPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        gesture.delegate = self
        articleImageView.isUserInteractionEnabled = true
        articleImageView.addGestureRecognizer(gesture)
        articlePresenter = ArticlesPresenter()
        articlePresenter?.delegate = self
        
        
        articleUpdate = Articles()
        articleUpdate = article
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        dynamicHeightTextView(self.articleDescriptionTextView)
        
        if status == 2{//update 
            setInformation(title: "Update Article", article: article!, buttonTitle: "Update")
            if let _ = img {
                self.articleImageView.image = self.img
            }
        }
        if status == 1 {// veiw detail
            self.articleDescriptionTextView.isEditable = false
            self.articleDescriptionTextView.textColor = .gray
            self.articleTitleTextView.isEditable = false
            self.articleTitleTextView.textColor = .gray
            self.articleImageView.isUserInteractionEnabled = false
            
            setInformation(title: "Veiw Article", article: article!, buttonTitle: "Done")
        }
    }
    
    @IBAction func modifyArticle(_ sender: Any) {
        
        if status == 0{//add new
            let article = Articles()
            if img == nil {
                article.title = self.articleTitleTextView.text
                article.description = self.articleDescriptionTextView.text
                article.image = "https://i.ytimg.com/vi/sBrjJNrMJpE/hqdefault.jpg"
                article.status = "added"
                KVLoading.show(animated: false)
                self.articlePresenter?.addAriticle(article: article)
            }
            else{
                Alamofire.upload(multipartFormData: { (form) in
                    form.append(self.img.jpegData(compressionQuality: 0.2)!, withName: "FILE", fileName : ".jpg", mimeType: "image/jpg")
                }, to: "http://api-ams.me/v1/api/uploadfile/single", encodingCompletion: {
                    result in
                    switch result {
                    case .success(let upload, _, _):
                        upload.responseJSON(completionHandler: { (imageInfo) in
                            let imageObjInfo = imageInfo.result.value as! [String: Any]
                            self.imageUrl = imageObjInfo["DATA"] as? String ?? "https://i.ytimg.com/vi/sBrjJNrMJpE/hqdefault.jpg"
                            let article = Articles()
                            article.title = self.articleTitleTextView.text
                            article.description = self.articleDescriptionTextView.text
                            article.image = self.imageUrl
                            article.status = "added"
                            KVLoading.show(animated: false)
                            self.articlePresenter?.addAriticle(article: article)
                        })
                    case .failure(_):
                        self.imageUrl = "https://i.ytimg.com/vi/sBrjJNrMJpE/hqdefault.jpg"
                        print("Insert Article not success")
                    }
                })
            }
        }
        if status == 1{//view detail
            self.navigationController?.popViewController(animated: true)
        }
        if status == 2 {//update
           
            
            if img == nil {
                articleUpdate?.image = self.article?.image
                self.articlePresenter?.updateArticle(article: articleUpdate!)
            }
            else{
                
                Alamofire.upload(multipartFormData: { (form) in
                    form.append(self.img.jpegData(compressionQuality: 0.2)!, withName: "FILE", fileName : ".jpg", mimeType: "image/jpg")
                }, to: "http://api-ams.me/v1/api/uploadfile/single", encodingCompletion: {
                    result in
                    switch result {
                    case .success(let upload, _, _):
                        upload.responseJSON(completionHandler: { (imageInfo) in
                            let imageObjInfo = imageInfo.result.value as! [String: Any]
                            self.imageUrl = imageObjInfo["DATA"] as? String ?? "https://i.ytimg.com/vi/sBrjJNrMJpE/hqdefault.jpg"
                            self.articleUpdate?.image = self.imageUrl
                            self.articlePresenter?.updateArticle(article: self.articleUpdate!)
                        })
                    case .failure(_):
                        self.imageUrl = "https://i.ytimg.com/vi/sBrjJNrMJpE/hqdefault.jpg"
                        print("Insert Article not success")
                    }
                })
            }
            
        }
    }

    @objc func tapDetected() {
        let alert = UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Choose Photo", style: .default, handler: { (_) in
            print("Choose Photo")
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true){  }
        }))
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: { (_) in
            print("Take Photo")
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true){  }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print("image was choose")
        
        let img = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        self.articleImageView.image = img
        self.dismiss(animated: true, completion: nil)
        self.img = img
    }
    
    func setInformation(title: String, article : Articles, buttonTitle: String)  {
        self.titleLable.text = title
        self.articleDescriptionTextView.text = article.description
        self.articleTitleTextView.text = article.title
        let url = URL(string: article.image ?? "DefaultImage")
        self.articleImageView.kf.setImage(with: url, placeholder: UIImage(named: "DefaultImage"))
        self.modifyButton.title = buttonTitle
    }
    
    func dynamicHeightTextView(_ textView: UITextView) {
        //textView.translatesAutoresizingMaskIntoConstraints = true
        textView.sizeToFit()
        textView.isScrollEnabled = false
    }
    
    
    
}

extension ModifyArticleViewController: ArticlesPresenterDelegate{
    func didGetArticleSuccess(articles: [Articles]) {   }
    
    func didAddNewArticleSuccess(msg: String) {
        print("Add sucess")
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func didUpdateArticleSuccess(msg: String) {
       self.navigationController?.popViewController(animated: true)
    }
    
    func didDeleteArticleSuccess(msg: String) {     }
    
    
    
}
