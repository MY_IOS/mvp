

import Foundation
import ObjectMapper

//Article Model
class  Articles:Mappable {
    var id:Int = 0
    var title: String?
    var description: String?
    var createdDate: String?
    var status: String?
    var image: String?
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["ID"]
        title <- map["TITLE"]
        description <- map["DESCRIPTION"]
        createdDate <- map["CREATED_DATE"]
        status <- map["STATUS"]
        image <- map["IMAGE"]
    }
}
