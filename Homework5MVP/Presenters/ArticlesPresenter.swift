
import Foundation

protocol ArticlesPresenterDelegate {
    func didGetArticleSuccess(articles: [Articles])
    func didAddNewArticleSuccess(msg: String)
    func didUpdateArticleSuccess(msg: String)
    func didDeleteArticleSuccess(msg: String)
}


class ArticlesPresenter:ArticlesServiceDelegate {
    var delegate: ArticlesPresenterDelegate?
    var articlesService: ArticlesService?
    var urlImage: String?
   
    init() {
        self.articlesService = ArticlesService()
        self.articlesService?.delegate = self
        
    }
    
    func getArticles(page: Int, limit: Int) {
        self.articlesService?.getArticles(page: page, limit: limit)
    }
    
    func addAriticle(article: Articles)  {
        self.urlImage = self.articlesService?.imageUrl
        self.articlesService?.addAriticle(article: article)
    }
    
    func updateArticle(article: Articles  )  {
        self.articlesService?.updateArticle(article: article)
    }
    func deleteArticle(articleId: Int) {
        self.articlesService?.deleteArticle(articleId: articleId)
    }
    
//    func uploadImage() -> String {
//        return (self.articlesService?.uploadImage())!
//    }
    
    
    
    
    
    
    
    func didGetArticleSuccess(articles: [Articles]) {
        self.delegate?.didGetArticleSuccess(articles: articles)
    }
    
    func didAddNewArticleSuccess(msg: String) {
        self.delegate?.didAddNewArticleSuccess(msg: msg)
    }
    
    func didUpdateArticleSuccess(msg: String) {
        self.delegate?.didUpdateArticleSuccess(msg: msg)
    }
    
    func didDeleteArticleSuccess(msg: String) {
        self.delegate?.didDeleteArticleSuccess(msg: msg)
    }
    
}

